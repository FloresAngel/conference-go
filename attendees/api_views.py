# force you to require a HTTP method.
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
# steps for JSON Library
from common.json import ModelEncoder
from .models import Attendee

from events.models import Conference
import json

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "created",
        "updated",
        "max_presentations",
        "max_attendees",
    ]

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        return JsonResponse(
            {"attendees": attendees},# Convert QuerySet to list before serialization
            encoder=AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)
    # Get the Conference object and put it in the content dict
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )
    #  content["conference_id"] = conference_id  # Ensure conference_id is set correctly
    # Create a new attendee
    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )

# def api_show_attendee(request, id):
    # attendee = Attendee.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "email":attendee.email,
    #         "name":attendee.name,
    #         "company_name":attendee.company_name,
    #         "created":attendee.created,
    #         "conference": {
    #             "name": attendee.conference.name,
    #             "href": attendee.conference.get_api_url(),
    #         }
    #     }
    # )
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return JsonResponse({"error": "Attendee not found"}, status=404)

    attendee_data = {
        "email": attendee.email,
        "name": attendee.name,
        "company_name": attendee.company_name,
        "created": attendee.created,
        "conference": {
            "name": attendee.conference.name,
            "href": attendee.conference.get_api_url(),
        }
    }
    return JsonResponse(attendee_data, encoder=ModelEncoder, safe=False)
