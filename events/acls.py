import json
import requests
from .keys import PEXELS_API_KEY

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f'{city} {state}',
        "per_page": 1,
    }
    url = 'https://api.pexels.com/v1/search'
    res = requests.get(url, params=params, headers=headers)
    content = json.loads(res.content)

    photo = {}
    try:
        photo["picture_url"] = content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        photo["picture_url"] = None
    return photo

    # Create a dictionary for the headers to use
    # in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):
    pass
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
